"""
Author: Alain Moré Maceda
Scraper de productos Julio Cepeda MX
"""
import csv
import json
import logging
import time
import warnings
from datetime import datetime

import jinja2
import pandas as pd
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from snowflake.sqlalchemy import URL
from sqlalchemy import create_engine

warnings.filterwarnings("ignore")
logging.basicConfig(
    level=logging.INFO,
    filename="log_scraper.log",
    filemode="a",
    format="%(asctime)s :: %(levelname)s :: %(message)s",
)
log = logging.getLogger("__name__")

chrome_opt = webdriver.ChromeOptions()
prefs = {"profile.default_content_setting_values.notifications": 2}
chrome_opt.add_experimental_option("prefs", prefs)
chrome_opt.add_experimental_option("excludeSwitches", ["enable-automation"])
chrome_opt.add_experimental_option("useAutomationExtension", False)
chrome_opt.add_argument(
    "user-agent=Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:63.0) Gecko/20100101 Firefox/63.0"
)
driver = webdriver.Chrome(chrome_options=chrome_opt)
driver.maximize_window()
wait = WebDriverWait(driver, 45)
actions = ActionChains(driver)

existing_products = []
new_products_to_add = []


def get_existing_products(connection):

    select = """
        SELECT SKU FROM GLOBAL_ECOMMERCE.JULIO_CEPEDA_PRODUCTS;
    """
    template = jinja2.Environment(loader=jinja2.BaseLoader).from_string(select)
    formatted_template = template.render()
    log.info(formatted_template)
    try:
        df_existing_products = pd.read_sql_query(
            con=connection, sql=formatted_template)
        log.info(df_existing_products)
        for element in df_existing_products.values.tolist():
            existing_products.append(element[0])
    except Exception as ex:
        log.error(ex)


def get_dom(url):
    """
    gets the DOM of the requested url using selenium
    """
    try:
        driver.get(url)
    except:
        try:
            time.sleep(60)
            driver.get(url)
        except:
            log.info("Error con alert")


def insert_product(connection, product):
    """
    Inserts current processed product into Snowflake
    """
    insert = """
        INSERT INTO GLOBAL_ECOMMERCE.JULIO_CEPEDA_PRODUCTS 
            (CATEGORY,URL,NAME,PRICE,SALE_PRICE,PICTURES,SUMMARY,SKU,STOCK,SCRAPE_DATETIME)
        VALUES(
            '{{prod_category}}',
            '{{prod_url}}',
            '{{prod_name}}',
            {{price}},
            {{sale_price}},
            '{{pictures}}',
            '{{prod_desc}}',
            '{{sku}}',
            '{{stock}}',
            '{{scrape_datetime}}'  
        )     
    """
    template = jinja2.Environment(loader=jinja2.BaseLoader).from_string(insert)
    formatted_template = template.render(**product)
    log.info(formatted_template)
    try:
        df_insert = pd.read_sql_query(con=connection, sql=formatted_template)
        log.info("Insert OK")
        log.info(df_insert)
    except Exception as ex:
        log.info("Insert ERROR")
        log.error(ex)


def start_scraping(category_name, url, connection):
    """
    Scraping main function, orchestrates full process
    """
    # En este bloque se obtienen los productos de la categoria.
    log.info("")
    log.info("")
    log.info(">>>>>> CATEGORIA %s", category_name)
    get_dom(url)
    driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
    cat_soup = BeautifulSoup(driver.page_source, "html.parser")
    prod_category = category_name

    paginas = 1
    try:
        more_pages_element = cat_soup.find(
            "ul", {"class": "items pages-items"})
        if more_pages_element:
            last_page_element = more_pages_element.find_all(
                "li", {"class": "item"})[-2].get_text().strip()
            paginas = int(list(filter(str.isdigit, last_page_element))[0])
            log.info("Página %i", paginas)
        else:
            log.info("Sin más páginas")
    except:
        log.exception("ERROR AL BUSCAR ARTICULOS")

    log.info("Páginas: %i", paginas)

    products = []
    for pagina in range(1, paginas+1):
        get_dom(url + "?p=" + str(pagina))
        cat_soup = BeautifulSoup(driver.page_source, "html.parser")

        prod_url_tags = cat_soup.find_all("a", {"class", "product-item-link"})
        if prod_url_tags:
            log.info("")
            log.info("Categoria %s, Pagina %i, Total productos: %i",
                     prod_category, pagina, len(prod_url_tags))
            log.info("")
            for prod_url_tag in prod_url_tags:
                prod_url = prod_url_tag["href"]

                product_info = {
                    "prod_category": prod_category, "prod_url": prod_url}

                if product_info["prod_url"] not in new_products_to_add:
                    products.append(product_info)
                    log.info("Nuevo producto agregado: ")
                    log.info(product_info)
                    new_products_to_add.append(product_info["prod_url"])
                else:
                    log.info("Producto duplicado: ")
                    log.info(product_info)
        else:
            log.info("SIN PRODUCTOS")

    log.info("-----")
    log.info("")
    log.info("")
    log.info("")

    # Obtener detalles desde la página del producto
    for product in products:
        get_dom(product["prod_url"])
        time.sleep(1)
        prod_soup = BeautifulSoup(driver.page_source, "html.parser")

        valid_product = True

        # Disponibilidad
        stock = ""
        stock_tag = prod_soup.find("div", {"class": "stock available"})
        if stock_tag:
            stock_tmp = stock_tag.find_all("span")[-1]
            stock = stock_tmp.text.strip().replace("'", "")
            product["stock"] = stock
        else:
            log.info("No se pudo obtener disponibilidad - stock")

        # NAME
        prod_name = ""
        prod_name_tag = prod_soup.find("span", {"itemprop": "name"})
        if prod_name_tag:
            prod_name = prod_name_tag.text.strip().replace("'", "")
            product["prod_name"] = prod_name
        else:
            log.info("No se pudo obtener prod_name")
            valid_product = False

        # DESCRIPTION
        prod_desc = ""
        tmp_sku = ""
        prod_desc_tag = prod_soup.find("div", {"itemprop": "description"})
        if prod_desc_tag:
            prod_desc_tmp = (
                prod_desc_tag.text.strip()
                .replace("'", "")
                .replace("\n", " ")
                .replace("\r", "")
            )
            if "Codigo:" in prod_desc_tmp:
                prod_desc = prod_desc_tmp.split("Codigo:")[0].strip()
                tmp_sku = prod_desc_tmp.split("Codigo:")[1].strip()
            elif "codigo:" in prod_desc_tmp:
                prod_desc = prod_desc_tmp.split("codigo:")[0].strip()
                tmp_sku = prod_desc_tmp.split("codigo:")[1].strip()
            elif "CODIGO:" in prod_desc_tmp:
                prod_desc = prod_desc_tmp.split("CODIGO:")[0].strip()
                tmp_sku = prod_desc_tmp.split("CODIGO:")[1].strip()
            elif "Sku:" in prod_desc_tmp:
                prod_desc = prod_desc_tmp.split("Sku:")[0].strip()
                tmp_sku = prod_desc_tmp.split("Sku:")[1].strip()
            elif "SKU:" in prod_desc_tmp:
                prod_desc = prod_desc_tmp.split("SKU:")[0].strip()
                tmp_sku = prod_desc_tmp.split("SKU:")[1].strip()
            elif "sku:" in prod_desc_tmp:
                prod_desc = prod_desc_tmp.split("sku:")[0].strip()
                tmp_sku = prod_desc_tmp.split("sku:")[1].strip()
            elif "CODIGO" in prod_desc_tmp:
                prod_desc = prod_desc_tmp.split("CODIGO")[0].strip()
                tmp_sku = prod_desc_tmp.split("CODIGO")[1].strip()
            elif "Codigo" in prod_desc_tmp:
                prod_desc = prod_desc_tmp.split("Codigo")[0].strip()
                tmp_sku = prod_desc_tmp.split("Codigo")[1].strip()
            elif "codigo" in prod_desc_tmp:
                prod_desc = prod_desc_tmp.split("codigo")[0].strip()
                tmp_sku = prod_desc_tmp.split("codigo")[1].strip()
            elif "SKU " in prod_desc_tmp:
                prod_desc = prod_desc_tmp.split("SKU ")[0].strip()
                tmp_sku = prod_desc_tmp.split("SKU ")[1].strip()
            elif "sku " in prod_desc_tmp:
                prod_desc = prod_desc_tmp.split("sku ")[0].strip()
                tmp_sku = prod_desc_tmp.split("sku ")[1].strip()
            elif "Sku " in prod_desc_tmp:
                prod_desc = prod_desc_tmp.split("Sku ")[0].strip()
                tmp_sku = prod_desc_tmp.split("Sku ")[1].strip()
            else:
                prod_desc = prod_desc_tmp

            product["prod_desc"] = prod_desc
        else:
            log.info("No se pudo obtener prod_desc")

        # SKU
        sku = ""
        sku_tag = prod_soup.find("div", {"itemprop": "sku"})
        if sku_tag:
            sku = sku_tag.text.strip()
            if sku.isnumeric():
                log.info("SKU correcto %s", sku)
                product["sku"] = sku
            else:
                log.info("SKU Incorrecto, tratando de obtener de descripción...")
                if prod_desc:
                    if tmp_sku.isnumeric():
                        product["sku"] = tmp_sku
                        log.info("SKU de descripción: %s", tmp_sku)
                    else:
                        product["sku"] = "NA"
                        log.info("No se pudo obtener SKU 1.")
                        valid_product = False
                else:
                    product["sku"] = "NA"
                    log.info("No se pudo obtener SKU 2.")
                    valid_product = False

        else:
            log.info("No se pudo obtener sku")

        # PICTURES
        pictures = []
        time.sleep(1)
        pictures_parent_tag = prod_soup.find(
            "div", {"class": "fotorama__stage__shaft"}
        )
        if pictures_parent_tag:
            picture_tags = pictures_parent_tag.find_all(
                "img", {"class": "fotorama__img"}
            )
            if picture_tags:
                for pic_tag in picture_tags:
                    pictures.append(pic_tag["src"])

                pics_json = json.dumps(pictures)
                product["pictures"] = pics_json
            else:
                log.info("No se pudieron obtener imagenes")
        else:
            log.info("No se pudo obtener el parent de imagenes")

        # PRICE(S)
        price = 0.0
        sale_price = 0.0
        price_tag = prod_soup.find_all("span", {"class": "price"})
        if price_tag:
            price = float(
                price_tag[0].text.strip()
                .replace("'", "")
                .replace("$", "")
                .replace(",", "")
            )

            if len(price_tag) > 1:
                log.info("CON rebaja")
                sale_price = float(
                    price_tag[1].text.strip()
                    .replace("'", "")
                    .replace("$", "")
                    .replace(",", "")
                )
            else:
                log.info("SIN rebaja")
                sale_price = price

            product["price"] = price
            product["sale_price"] = sale_price
        else:
            log.info("No se pudo obtener precios")
            valid_product = False

        product["scrape_datetime"] = datetime.now()

        if valid_product and product["price"] != 0.0:
            if product["sku"] not in existing_products:
                insert_product(connection, product)
                existing_products.append(product["sku"])
                log.info("Producto insertado correctamente...")
            else:
                log.info("Producto ya insertado, ignorando...")
        else:
            log.info("PRODUCTO NO VALIDO: %s", product["prod_url"])

        log.info(product)
        log.info("")
        log.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
        log.info("")


def main():
    """
    Función main, ejecuta el proceso paso por paso
    """
    print("PROCESS STARTED!")
    snowflake_configuration = {
        "url": "hg51401.snowflakecomputing.com",
        "account": "hg51401",
        "user": "ALAIN.MORE@RAPPI.COM",
        "authenticator": "externalbrowser",
        "port": 443,
        "warehouse": "ECOMMERCE",
        "role": "GLOBAL_ECOMMERCE_WRITE_ROLE",
        "database": "fivetran"
    }
    engine = create_engine(URL(**snowflake_configuration))
    connection = engine.connect()
    get_existing_products(connection)
    try:
        with open("categories.csv", "r") as csvfile:
            csvreader = csv.DictReader(csvfile)
            for row in csvreader:
                cat_name = row["category_name"]
                cat_url = row["category_url"]
                start_scraping(cat_name, cat_url, connection)

        driver.close()
        print("PROCESS ENDED OK!")
    finally:
        connection.close()
        engine.dispose()
        log.info("Engine y connection cerrados")


if __name__ == "__main__":
    main()
